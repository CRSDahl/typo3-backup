#!/bin/sh

mysqldump --no-tablespaces -h$DB_HOST -u$DB_USER -p$DB_PASSWORD $DB_NAME > /home/jobberuser/backup/db_dump.sql

set -o errexit
set -o nounset
set -o pipefail

readonly SOURCE_DIR="/home/jobberuser/backup"
readonly BACKUP_DIR="/home/jobberuser/backups"
readonly DATETIME="$(date '+%Y-%m-%d_%H:%M:%S')"
readonly BACKUP_PATH="${BACKUP_DIR}/${DATETIME}"
readonly LATEST_LINK="${BACKUP_DIR}/latest"

mkdir -p "${BACKUP_DIR}"

rsync -av --delete \
  "${SOURCE_DIR}/" \
  --link-dest "${LATEST_LINK}" \
  --exclude-from="/home/jobberuser/rsyncexclude" \
  "${BACKUP_PATH}"

rm -rf "${LATEST_LINK}"
ln -s "${BACKUP_PATH}" "${LATEST_LINK}"